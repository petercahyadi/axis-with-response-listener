package org.apache.axis.transport.http;

public interface AxisHTTPSenderResponseListener {

    void responseReceived(String response);

}
